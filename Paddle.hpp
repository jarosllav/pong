#pragma once
#include <SFML/Window/Keyboard.hpp>
#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/RenderWindow.hpp>

class Paddle {
public:
	Paddle(const sf::Vector2f& position, const sf::Vector2f& size, const sf::Keyboard::Key& left = sf::Keyboard::Left, const sf::Keyboard::Key& right = sf::Keyboard::Right) :
		m_keyLeft(left),
		m_keyRight(right)
	{
		m_shape.setPosition(position);
		m_shape.setSize(size);
		m_shape.setFillColor(sf::Color::White);
	}

	void Draw(sf::RenderWindow* window) {
		window->draw(m_shape);
	}
	void Move(const sf::Vector2f& vel) {
		m_shape.move(vel);
	}

	inline void SetPosition(const sf::Vector2f& position) { m_shape.setPosition(position); }

	inline sf::Vector2f GetPosition() const { return this->m_shape.getPosition(); }
	inline sf::Vector2f GetSize() const { return this->m_shape.getSize(); }
	inline sf::Keyboard::Key GetKeyLeft() const { return this->m_keyLeft; }
	inline sf::Keyboard::Key GetKeyRight() const { return this->m_keyRight; }
	inline sf::RectangleShape GetShape() const { return this->m_shape; }

private:
	sf::RectangleShape m_shape;
	sf::Keyboard::Key m_keyLeft;
	sf::Keyboard::Key m_keyRight;

};