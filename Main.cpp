#include <iostream>

#include <SFML/Graphics.hpp>

#include "Ball.hpp"
#include "Paddle.hpp"

int main() {

	sf::Vector2f BALL_SPEED = { 0.05f, 0.05f };

	sf::RenderWindow window(sf::VideoMode(800, 600, 32), "Ping-pong!");
	auto winSize = window.getSize();

	// GUI
	sf::Font font;
	font.loadFromFile("data/font.ttf");

	sf::Vector2u score(0, 0);
	sf::Text text_score("Score: 0-0", font, 64);
	text_score.setPosition({ winSize.x / 2 - text_score.getLocalBounds().width / 2.f, winSize.y / 2 -  text_score.getLocalBounds().height});
	text_score.setFillColor(sf::Color(255, 255, 255, 150));

	sf::Text text_player1("Player 1", font, 14);
	text_player1.setPosition({ 5.f, 5.f });

	sf::Text text_player2("Player 2", font, 14);
	text_player2.setPosition({ 5.f, winSize.y - 20.f });

	Ball ball(sf::Vector2f(15.f, 15.f));
	Paddle paddle({ winSize.x / 2.f - 150.f / 2.f, winSize.y - 35.f }, {125.f, 10.f});
	Paddle paddle2({ winSize.x / 2.f - 150.f / 2.f, 25.f }, { 125.f, 10.f }, sf::Keyboard::A, sf::Keyboard::D);

	ball.SetPosition({ winSize.x / 2.f - ball.GetSize().x / 2.f, winSize.y / 2.f - ball.GetSize().y / 2.f });

	sf::Event evt;
	while (window.isOpen()) {
		while (window.pollEvent(evt)) {
			if (evt.type == sf::Event::Closed)
				window.close();
			else if (evt.type == sf::Event::MouseMoved) {
				auto mouse = sf::Mouse::getPosition(window);
			}
		}

		// Move paddles
		auto keyLeft = paddle.GetKeyLeft();
		auto keyRight = paddle.GetKeyRight();
		if (sf::Keyboard::isKeyPressed(keyLeft))
			paddle.Move({ -0.1f, 0.f });
		if (sf::Keyboard::isKeyPressed(keyRight))
			paddle.Move({ 0.1f, 0.f });

		keyLeft = paddle2.GetKeyLeft();
		keyRight = paddle2.GetKeyRight();
		if (sf::Keyboard::isKeyPressed(keyLeft))
			paddle2.Move({ -0.1f, 0.f });
		if (sf::Keyboard::isKeyPressed(keyRight))
			paddle2.Move({ 0.1f, 0.f });

		// Clamp position
		auto position = paddle.GetPosition();
		if (position.x < 0)
			paddle.SetPosition({ 0.f, position.y });
		else if (position.x + paddle.GetSize().x > winSize.x)
			paddle.SetPosition({ winSize.x - paddle.GetSize().x, position.y });

		position = paddle2.GetPosition();
		if (position.x < 0)
			paddle2.SetPosition({ 0.f, position.y });
		else if (position.x + paddle2.GetSize().x > winSize.x)
			paddle2.SetPosition({ winSize.x - paddle2.GetSize().x, position.y });

		auto ball_pos = ball.GetPosition();
		auto ball_size = ball.GetSize();
		if (ball_pos.x <= 0 || ball_pos.x + ball_size.x >= winSize.x)
			BALL_SPEED.x = -BALL_SPEED.x;

		if (ball_pos.y <= 0 || ball_pos.y + ball_size.y >= winSize.y) {
			ball_pos.y <= 0 ? score.y += 1 : score.x += 1;
			text_score.setString("Score: " + std::to_string(score.x) + "-" + std::to_string(score.y));

			ball.SetPosition({ winSize.x / 2.f - ball.GetSize().x / 2.f, winSize.y / 2.f - ball.GetSize().y / 2.f });
		}

		if (ball.IsCollide(paddle) || ball.IsCollide(paddle2)) {
			BALL_SPEED.y = -BALL_SPEED.y;
		}

		ball.Move(BALL_SPEED);

		window.clear();

		// GUI
		window.draw(text_score);
		window.draw(text_player1);
		window.draw(text_player2);

		ball.Draw(&window);
		paddle.Draw(&window);
		paddle2.Draw(&window);
		window.display();
	}

	return EXIT_SUCCESS;
}