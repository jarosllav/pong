#pragma once
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/RenderWindow.hpp>

#include "Paddle.hpp"

class Ball {
public:
	Ball(const sf::Vector2f& size)
	{
		m_shape.setFillColor(sf::Color::Red);
		m_shape.setSize(size);
	}

	void Draw(sf::RenderWindow* window) {
		window->draw(m_shape);
	}
	void Move(const sf::Vector2f& vel) {
		m_shape.move(vel);
	}
	bool IsCollide(const Paddle& paddle) {
		if (paddle.GetShape().getGlobalBounds().intersects(this->m_shape.getGlobalBounds()))
			return true;
		return false;
	}

	inline void SetPosition(const sf::Vector2f& position) { m_shape.setPosition(position); }

	inline sf::Vector2f GetPosition() const { return this->m_shape.getPosition(); }
	inline sf::Vector2f GetSize() const { return this->m_shape.getSize(); }
	inline sf::RectangleShape GetShape() const { return this->m_shape; }
private:
	sf::RectangleShape m_shape;

};